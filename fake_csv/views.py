from django.contrib.auth import logout
from django.shortcuts import render, redirect


# Create your views here.
def home_view(request):
    """
    Displays home page.
    """
    return render(request, 'index.html')


def logout_view(request):
    logout(request)
    return redirect('home')
