Fake-csv source code
=============================

To run locally, do the usual:
-----------------

#. Create a Python 3.8 virtualenv::

    python3 -m venv env
    source env/bin/activate

#. Install dependencies::

    pip install -r requirements.txt
    npm install

#. Create tables::

    ./manage.py migrate

#. Create a superuser::

   ./manage.py createsuperuser

#. Populate tables from fixtures::

    ./manage.py loaddata schema

#. Run the celery server::

    celery -A fake_csv worker -l info

#. Run the python server::

    ./manage.py runserver 8000

