from django.contrib import admin

from schema.models import SchemaType, Schema, SchemaColumn, SchemaItem


# Register your models here.
class SchemaTypeAdmin(admin.ModelAdmin):
    fields = ('name', 'type')
    list_display = ('name', 'type')
    list_filter = ('type',)


class SchemaColumnInline(admin.TabularInline):
    model = SchemaColumn
    fields = ('order', 'min', 'max', 'schema_type')
    extra = 5


class SchemaItemInline(admin.TabularInline):
    model = SchemaItem
    fields = ('get_created', 'status', 'file')
    extra = 1
    readonly_fields = ('get_created',)

    @admin.display(description='Created')
    def get_created(self, obj):
        return obj.created.strftime('%d:%m:%Y %H:%M')


class SchemaAdmin(admin.ModelAdmin):
    date_hierarchy = 'created'
    fieldsets = (
        (None, {
            'fields': ('name', 'column_separator', 'string_character', 'get_created', 'get_updated')
        }),
    )
    list_display = ('name', 'get_created', 'get_updated')
    search_fields = ['name']
    readonly_fields = ('get_created', 'get_updated')
    inlines = [SchemaColumnInline, SchemaItemInline]
    ordering = ['-created']

    @admin.display(description='Created')
    def get_created(self, obj):
        return obj.created.strftime('%d:%m:%Y %H:%M')

    @admin.display(description='Updated')
    def get_updated(self, obj):
        return obj.updated.strftime('%d:%m:%Y %H:%M')


admin.site.register(SchemaType, SchemaTypeAdmin)
admin.site.register(Schema, SchemaAdmin)
