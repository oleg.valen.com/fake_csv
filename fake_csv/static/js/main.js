$(document).ready(function () {

    let newColumnIndex = 0;
    let csrftoken = $.cookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $('body').on('click', '.add-column', function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: '/schema/post-schema-types'
        })
            .done((response) => {
                let i = newColumnIndex;
                newColumnIndex++;
                i = 'new_' + i;
                let options = ''

                let columns = $('.columns-container');
                for (let idx = 0; idx < response.length; idx++) {
                    options += `<option value='${response[idx].id}' data-type="${response[idx].type}">${response[idx].name}</option>`;
                }
                let html = `
                    <div class="columns">
                        <input class="input" type="hidden" name="schema_column-${i}-id" value="${i}">
                        <div class="column field control">
                            <input class="input" type="text" name="schema_column-${i}-name" value="${response[0].name}">
                        </div>
                        <div class="column field control">
                            <div class="select">
                                <select name="schema_column-${i}-schema_type_id">${options}</select>
                            </div>
                        </div>
                        <div class="column field control min" data-idx="${i}"></div>
                        <div class="column field control max" data-idx="${i}"></div>
                        <div class="column">
                            <div class="columns">
                                <div class="column field control">
                                    <input class="input" name="schema_column-${i}-order" type="number" value="0">
                                </div>
                                <div class="column">
                                    <a onclick="return confirm('Are you sure you want to delete this item?');">
                                        <button type="submit" name='delete_schema_column-${i}'>Delete</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>`
                columns.append(html)
            })
            .fail((error) => console.log(error))
    });

    $('body').on('change', 'select', function (e) {
        let type = $(this).find(':selected').data('type');
        let columns = $(this).closest('.columns');
        let min = columns.find('.column.min');
        let max = columns.find('.column.max');
        let idx = min.data('idx');
        if (['paragraph', 'pyint'].includes(type)) {
            min.html(`<input class="input" name="schema_column-${idx}-min" type="number" value="0">`);
            max.html(`<input class="input" name="schema_column-${idx}-max" type="number" value="0">`);
        } else {
            min.html('');
            max.html('');
        }
    });

    $('body').on('click', '.generate-data', function (e) {
        e.preventDefault()
        $.ajax({
            type: 'POST',
            url: '/schema/post_create_csv_file',
            data: {
                schema_id: $('input[name=schema_id]').val(),
                count: $('input[name=count]').val(),
            }
        })
            .done((response) => {
                let tbody = $('tbody');
                let html = `
                    <tr id="${response.task_id}">
                        <td>${response.schema_item_id}</td>
                        <td>${response.created}</td>
                        <td class="status"><span class="tag ${response.status_class}">${response.status_name}</span></td>
                    </tr>`
                tbody.append(html);
                updateStatus(response.task_id);

            })
            .fail((error) => console.log(error))
    })

    function updateStatus(taskId) {
        $.ajax({
            type: 'POST',
            url: `/schema/post_csv_file_task_status/${taskId}/`,
        })
            .done(response => {
                let tr = $('#' + response.task_id);
                let span = tr.find('span.tag');
                span.removeClass();
                span.addClass('tag ' + response.status_class);
                span.text(response.status_name)

                if (response.status == 'SUCCESS') {
                    let html = `
                        <td>
                            <a href="${response.file_name}">Download</a>
                        </td>`
                    tr.append(html)
                }

                if (response.status == "FAILURE" || response.status == 'SUCCESS') {
                    return false
                }
                setTimeout(function () {
                    updateStatus(response.task_id)
                }, 2000)
            })
            .fail(error => console.log(error))
    }

});
