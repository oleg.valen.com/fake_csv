import logging
import csv
import os
from pathlib import Path
from tempfile import NamedTemporaryFile

import boto3
from django.utils import timezone
from faker import Faker

from django.conf import settings

from fake_csv.celery import app
from schema.models import SchemaItem, Schema, PARAGRAPH, PYINT

logger = logging.getLogger(__name__)


@app.task
def create_csv_file(schema_id, schema_item_id, count):
    """
    Creates csv file filled with Faker.
    Everything in the single function because of NamedTemporaryFile -
    not delete temp file if creating/saving are in different functions.
    """
    fake = Faker()

    schema = Schema.objects.get(pk=schema_id)
    schema_columns = schema.columns_types()

    temp_file = NamedTemporaryFile(suffix='.csv', prefix='', dir=settings.BASE_DIR.joinpath('schema'))
    path = temp_file.name

    with open(path, mode='w') as f:
        f = csv.writer(f, delimiter=schema.column_separator,
                       quotechar=schema.string_character,
                       quoting=csv.QUOTE_MINIMAL)
        f.writerow(list(schema_columns.values_list('name', flat=True)))
        for i in range(0, count):
            lst = []
            for column in schema_columns:
                if column.schema_type.type == PARAGRAPH:
                    item = getattr(fake, column.schema_type.type)(column.max)
                elif column.schema_type.type == PYINT:
                    item = getattr(fake, column.schema_type.type)(column.min, column.max)
                else:
                    item = getattr(fake, column.schema_type.type)()
                lst.append(item)
            f.writerow(lst)

    schema_item = SchemaItem.objects.get(pk=schema_item_id)
    logger.debug('File (1): {} {}'.format(timezone.now(), str(Path(path).exists())))
    with Path(path).open(mode='rb') as f:
        s3_path = _upload_file(path)
        if s3_path:
            schema_item.file = s3_path
            schema_item.save()
        logger.debug('File (2): {} {}'.format(timezone.now(), str(Path(path).exists())))
        logger.debug('File (2): {} {}'.format(timezone.now(), str(Path(path).exists())))


def _upload_file(path):
    s3 = boto3.client('s3')
    s3_bucket = os.environ.get('S3_BUCKET')
    file_name = Path(path).name
    try:
        s3.upload_file(path, s3_bucket, file_name)
        return 'https://s3.eu-central-1.amazonaws.com/{}/{}'.format(s3_bucket, file_name)
    except Exception as e:
        logging.error(e)
    return None
