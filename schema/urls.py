from django.urls import path
from . import views

app_name = 'schema'
urlpatterns = [
    path('', views.list_view, name='list_view'),
    path('create', views.create_view, name='create_view'),
    path('<int:schema_id>/', views.detail_view, name='detail_view'),
    path('update/<int:schema_id>/', views.update_view, name='update_view'),
    path('delete', views.delete_view),
    path('post-schema-types', views.post_schema_types_view, name='post_schema_types_view'),
    # path('download/<int:schema_id>/<int:schema_item_id>/<str:file_name>/', views.download_view, name='download_view'),

    path('post_create_csv_file', views.post_create_csv_file, name='post_create_csv_file'),
    path('post_csv_file_task_status/<uuid:task_id>/', views.post_csv_file_task_status,
         name='post_csv_file_task_status'),
]
