import os

from django.db import models
from django.utils import timezone

NAME = 'name'
JOB = 'job'
EMAIL = 'email'
DOMAIN_NAME = 'domain_name'
PHONE_NUMBER = 'phone_number'
COMPANY = 'company'
PARAGRAPH = 'paragraph'
PYINT = 'pyint'
ADDRESS = 'address'

SCHEMA_TYPES = [
    (NAME, 'Name'),
    (JOB, 'Job'),
    (EMAIL, 'Email'),
    (DOMAIN_NAME, 'Domain name'),
    (PHONE_NUMBER, 'Phone number'),
    (COMPANY, 'Company'),
    (PARAGRAPH, 'paragraph'),
    (PYINT, 'Int'),
    (ADDRESS, 'Address'),
]

COMMA = ','
SEMICOLON = ';'
TAB = '\t'
COLUMN_SEPARATORS = [
    (COMMA, 'Comma (,)'),
    (SEMICOLON, 'Semicolon (;)'),
    (TAB, 'Tab ( )'),
]

DOUBLE_QUOTE = '"'
STRING_CHARACTERS = [
    (DOUBLE_QUOTE, 'Double-quote (")'),
]

FAILURE = 'FAILURE'
PENDING = 'PENDING'
SUCCESS = 'SUCCESS'
STATUSES = [
    (FAILURE, 'Failure'),
    (PENDING, 'Processing'),
    (SUCCESS, 'Ready')
]

STATUSES_DESC = {
    FAILURE: {'name': 'Failure', 'css_class': 'is-danger'},
    PENDING: {'name': 'Processing', 'css_class': 'is-info'},
    SUCCESS: {'name': 'Ready', 'css_class': 'is-success'},
}


# Create your models here.
class SchemaType(models.Model):
    name = models.CharField(max_length=128, blank=False)
    type = models.CharField(max_length=16, choices=SCHEMA_TYPES, blank=False, verbose_name='Name of Faker\'s function')

    def __str__(self):
        return self.name


class Schema(models.Model):
    name = models.CharField(max_length=128, blank=False)
    column_separator = models.CharField(max_length=1, default=COMMA, choices=COLUMN_SEPARATORS)
    string_character = models.CharField(max_length=1, default=DOUBLE_QUOTE, choices=STRING_CHARACTERS)
    created = models.DateTimeField(default=timezone.now)
    updated = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)

    def columns_types(self):
        return self.columns.select_related('schema_type').all()


class SchemaColumn(models.Model):
    name = models.CharField(max_length=128, blank=False)
    order = models.PositiveSmallIntegerField(default=0)
    min = models.PositiveSmallIntegerField(default=0, verbose_name='For pyint schema type range')
    max = models.PositiveSmallIntegerField(default=0, verbose_name='For pyint, paragraph schema types range')
    schema = models.ForeignKey(Schema, related_name='columns', related_query_name='column', on_delete=models.CASCADE)
    schema_type = models.ForeignKey(SchemaType, related_name='columns', related_query_name='column', null=True,
                                    on_delete=models.SET_NULL)

    class Meta:
        ordering = ['order']

    def __str__(self):
        return '{}: {}'.format(self.pk, self.schema_type.name)


class SchemaItem(models.Model):
    created = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=16, choices=STATUSES, verbose_name='Celery status')
    file = models.FileField(upload_to='schema/media', blank=True)
    task_id = models.CharField(max_length=36, db_index=True, verbose_name='Celery task.id')
    schema = models.ForeignKey(Schema, related_name='items', related_query_name='item', on_delete=models.CASCADE)

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return '{} ({})'.format(self.schema.name, self.status)

    @property
    def filename(self):
        return self.file.name

    @property
    def status_name(self):
        return STATUSES_DESC[self.status]['name']

    @property
    def status_css_class(self):
        return STATUSES_DESC[self.status]['css_class']
