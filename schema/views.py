import logging
import os
from pathlib import Path

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect

from celery.result import AsyncResult
from django.utils import timezone

from schema.models import Schema, SchemaColumn, SchemaType, SchemaItem
from schema.models import COLUMN_SEPARATORS, STRING_CHARACTERS, PENDING, SUCCESS, STATUSES_DESC
from schema.tasks import create_csv_file

logger = logging.getLogger(__name__)


# Create your views here.
@login_required
def list_view(request):
    """
    Displays all schemas.
    """
    return render(request, 'list_view.html', {
        'schemas': Schema.objects.all(),
    })


@login_required
def create_view(request):
    """
    Creates a schema.
    """
    if request.method == 'POST':
        result = _delete_schema_column(request)
        if result is not None:
            return result
        schema = _update_or_create_schema(request)
        return _update_or_create_schema_columns(request, schema)

    return render(request, 'create_view.html', {
        'schema_types': SchemaType.objects.all(),
        'column_separator': [i[0] for i in COLUMN_SEPARATORS],
        'string_character': [i[0] for i in STRING_CHARACTERS],
    })


@login_required
def detail_view(request, schema_id):
    """
    Displays schema's data by schema_id.
    """
    schema = get_object_or_404(Schema, pk=schema_id)
    return render(request, 'detail_view.html', {
        'schema': schema,
        'schema_items': SchemaItem.objects.filter(schema=schema)
    })


@login_required
def update_view(request, schema_id):
    """
    Updates schema's data by schema_id.
    """
    schema = get_object_or_404(Schema, pk=schema_id)
    if request.method == 'POST':
        result = _delete_schema_column(request)
        if result is not None:
            return result
        _update_or_create_schema(request, schema)
        return _update_or_create_schema_columns(request, schema)

    return render(request, 'update_view.html', {
        'schema': schema,
        'schema_columns': schema.columns_types(),
        'schema_types': SchemaType.objects.all(),
        'column_separator': [i[0] for i in COLUMN_SEPARATORS],
        'string_character': [i[0] for i in STRING_CHARACTERS],
    })


@login_required
def delete_view(request):
    """
    Deletes schema by schema_id.
    """
    try:
        schema_id = [s for s in request.POST if 'schema_id' in str(s)][0].split('-')[1]
    except IndexError:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    schema = get_object_or_404(Schema, pk=schema_id)
    schema.delete()
    return redirect('schema:list_view')


"""
Downloads local file
@login_required
def download_view(request, schema_id, schema_item_id, file_name):
    schema_item = get_object_or_404(SchemaItem, pk=schema_item_id)
    if Path(schema_item.file.path).exists():
        with open(schema_item.file.path, 'rb') as f:
            response = HttpResponse(f.read(), content_type="text/csv")
            response['Content-Disposition'] = 'inline; filename=' + Path(schema_item.file.path).name
            return response
    raise Http404
"""


def post_schema_types_view(request):
    """
    Gets all possible schema types.
    """
    data = list(SchemaType.objects.all().values('id', 'name', 'type'))
    return JsonResponse(data, safe=False, status=200)


def post_create_csv_file(request):
    """
    Creates csv file with Celery task.
    """
    if request.method == 'POST':
        schema_id = int(request.POST['schema_id'])
        count = int(request.POST['count'])

        schema = Schema.objects.get(pk=schema_id)
        schema_item = SchemaItem.objects.create(status=PENDING, schema=schema)
        schema_item.save()
        schema_item_id = schema_item.id

        task = create_csv_file.delay(schema_id, schema_item_id, count)
        schema_item.task_id = task.id
        schema_item.save()
        return JsonResponse({
            'task_id': task.id,
            'created': schema_item.created.strftime('%d:%m:%Y %H:%M'),
            'schema_id': schema_id,
            'schema_item_id': schema_item.id,
            'status': schema_item.status,
            'status_name': STATUSES_DESC[schema_item.status]['name'],
            'status_class': STATUSES_DESC[schema_item.status]['css_class']
        }, status=202)


def post_csv_file_task_status(request, task_id):
    """
    Gets Celery task status, updates status on page.
    """
    if request.method == 'POST':
        task_id = str(task_id)
        task = AsyncResult(task_id)
        schema_item = SchemaItem.objects.get(task_id=task_id)
        if schema_item.file.name:
            logger.debug('File (4): {} {}'.format(timezone.now(), str(Path(schema_item.file.path).exists())))
        else:
            logger.debug('File (3): {} False'.format(timezone.now()))
        response = {
            'task_id': task_id,
            'status': task.status,
            'status_name': STATUSES_DESC[task.status]['name'],
            'status_class': STATUSES_DESC[task.status]['css_class'],
            'info': task.info,
        }
        if task.status == SUCCESS:
            schema_item.status = SUCCESS
            schema_item.save()
            kwargs = {
                'schema_id': schema_item.schema_id,
                'schema_item_id': schema_item.id,
                'file_name': schema_item.filename
            }
            response.update(kwargs)
        return JsonResponse(response, status=200)


def _delete_schema_column(request):
    matching = [s for s in request.POST if 'delete_schema_column' in str(s)]
    if matching:
        schema_column_id = matching[0].split('-')[1]
        if 'new_' in schema_column_id:
            return HttpResponseRedirect(request.path_info)
        schema_column = get_object_or_404(SchemaColumn, pk=schema_column_id)
        schema_column.delete()
        return HttpResponseRedirect(request.path_info)
    return None


def _update_or_create_schema(request, schema=None):
    kwargs = {s.split('-')[1]: request.POST['schema_form-' + s.split('-')[1]] for s in
              [s for s in request.POST if 'schema_form' in str(s)]}
    if schema is None:
        return Schema.objects.create(**kwargs)
    else:
        return Schema.objects.filter(pk=schema.pk).update(**kwargs)


def _update_or_create_schema_columns(request, schema):
    schema_columns = {}
    for s in [s for s in request.POST if str(s).startswith('schema_column')]:
        _, schema_column_id, field = s.split('-')
        schema_columns.setdefault(schema_column_id, {})
        schema_columns[schema_column_id].setdefault(field,
                                                    request.POST[
                                                        'schema_column-{}-{}'.format(schema_column_id, field)])
    for pk, kwargs in schema_columns.items():
        kwargs.update({'schema_id': schema.id})
        if 'new_' in pk:
            kwargs.pop('id')
            SchemaColumn.objects.create(**kwargs)
        else:
            schema_column = SchemaColumn.objects.filter(pk=pk)
            schema_column.update(**kwargs)

    return redirect('schema:list_view')
